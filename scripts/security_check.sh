#!/bin/bash
# Title security_check.sh
# Date: 2018/12/09
# Author: Sean Pankanin
# Purpose: checks for files with chmod 777 perms in file system
# Update: reworked 12/9 


#boiler plate debug stuff, reusing:
passedArg="$1"

#debug mode toggle
#else "noDebug"
debug="noDebug"
#debug="debug"

if [ "$debug" = "debug" ]; then
	echo "debug mode = debug"
	echo "arg count: $#"
	echo "first arg: $1"
	echo $passedArg
	echo "2nd arg: $2"
	echo "3rd arg: $3"
fi
#end boiler plate debug stuff


now=`date`
echo "*******************************" >> security_log.txt
echo "*** Security report for: $HOSTNAME " >> security_log.txt
echo "*** Report Date: $now " >> security_log.txt
echo >> security_log.txt

#best effort is below - note that if I understand this correctly
#then this is actually a partial answer, some other perms like
#007 or other weird ones would not be caught.
#I also tried a few others like '/o+xwr' but saw they returned pattern match of any, rather than all. Never quite got syntax right, but in the case of the OTHER file it does work. 

find / -perm 0777 -type d  1>>security_log.txt 2>/dev/null

#an alterative inmplemenation of just o+w was interesting as it came up with a few dev/queue files, which was next best implementaiton. Could then do some additional output of those files and checking. 

echo >> security_log.txt

#established working command via getent to get wheel users, then formatted slightly with
#cut to get rid of starting text, to only show users
#could be enhanced further to get one user per line with a for/while loop but calling this good for now
#getent group wheel | cut -d":" -f4
echo "Users isted below (comma delimited) have wheel (sudo-ers) accesss on this system" 1>>security_log.txt
getent group wheel | cut -d":" -f4 1>>security_log.txt




echo "**** End security report ******" >> security_log.txt
echo "*******************************" >> security_log.txt
