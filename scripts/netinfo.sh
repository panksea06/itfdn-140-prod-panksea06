#!/bin/bash
# Title netinfo.sh 
# Date: 2018/12/09
# Author: Sean Pankanin
# Purpose: displays some network info, see comments/help 
# Update: reworked 12/9 


#Gather needed data here:

ipAddr=`ip addr show ens192 | grep 'inet ' | awk '{print $2}'| cut -d/ -f1`
macAddr=`ip addr show | grep "link/ether" | awk '{print $2}'`
passedArg="$1"

#debug mode toggle
#else "noDebug"
debug="noDebug"
#debug="debug"

if [ "$debug" = "debug" ]; then
	echo "debug mode = debug"
	echo "arg count: $#"
	echo "first arg: $1"
	echo $passedArg
	echo "2nd arg: $2"
	echo "3rd arg: $3"
fi




if [ "$passedArg" == "help" ]
	then
	echo "Usage This script expects either ip, mac or help as an argument."
	exit 0
fi

if [ "$passedArg" == "mac" ] 
	then
	echo $macAddr
	exit 0
fi

echo $ipAddr
