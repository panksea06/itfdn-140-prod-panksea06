#!/bin/bash
# Title rot13.sh
# Date: 2018/10/17
# Author: Sean Pankanin
# Purpose: rotates a single word by rot13 (aka, shifts characters right by 13 as super trival encoding) 
# Update: 

#debug mode toggle
debug="debug"
#else "noDebug"

if [ debug = "debug" ]; then
	echo "debug mode = debug"
else
	echo "debug mode = noDebug" 
fi

echo "This script will encode your single word of input using ROT13"
echo "Hello, enter your word for encoding: "
read rawWord

upperWord=${rawWord^^}

#debug line
echo "$upperWord"

#array of alphabet, for rotation

ARRAY=("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","Y","W","X","Y","Z")

rotArray=("N","O","P","Q","R","S","T","U","Y","W","X","Y","Z","A","B","C","D","E","F","G","H","I","J","K","L","M")


length=${#upperWord}
#debug line
#echo $length
#substr=${upperWord:2:1}

#echo $substr

for (( i=0; i<$length; i++))
	do
	echo "entered loop"
	echo "index is $i"
	
	substr=${upperWord:$i:1} 
	echo $substr
	
	#iterate through normal alpha array to get alphaIndex
	alphaIndex=-1 
	currentPos=0
	z=0
	while [ $alphaIndex -lt 0 ]
		do
		z=0
		for (( z=0; z<=27; z++ ))
			do
			#echo "lastest loop"
			if [ "${substr}" == "${ARRAY[$z]}" ]
				then  
				echo "match $substr at $z"
				alphaIndex=$z
			fi
			done 
		done
	
	echo "alphaIndex is $alphaIndex"
	
	rotChar = ${rotArray[$alphaIndex]}
	echo "ROT13 is $rotChar "
	
done







