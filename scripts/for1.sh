#!/bin/bash
# Title for1.sh 
# Date: 2018/12/09
# Author: Sean Pankanin
# Purpose: simple for loop demo
# Update: 



echo "Hello, enter your name: "
read name

echo "How many times would you like to be encouraged? (positive integer only)"
read number

#same as other turn in, but implemented with For rather than while
#Had originally tried implementing input String -> Rot13 output string but that proveed exceedingly difficult in bash since arrays are less fun to reference and compare to

for (( counter=0; counter<$number; counter++ )); do 
	time=`date`
	echo "$time :  $name, you can do it!" 
	sleep 5
	done



