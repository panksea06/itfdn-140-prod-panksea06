#!/bin/bash
# Title loop1.sh 
# Date: 2018/12/09
# Author: Sean Pankanin
# Purpose: Simple while loop example 
# Update: 

#debug mode toggle
debug="debug"
#else "noDebug"

if [ debug = "debug" ]; then
	echo "debug mode = debug"
else
	echo "debug mode = noDebug" 
fi


echo "Hello, enter your name: "
read name

echo "How many times would you like to be encouraged? (positive integer only)"
read number

counter="0"

while [ $counter -lt $number ]; do 
	time=`date`
	echo "$time :  $name, you can do it!" 
	counter=$(( $counter + 1 ))
	sleep 5
	done



