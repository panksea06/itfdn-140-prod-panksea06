#!/bin/bash
# Title  recall script
# Date: 2018/12/10
# Author: Sean Pankanin
# Purpose: recall tips and notes from store db file
# Update: 


# variables
#path to store.db data file
store_data="$HOME/repo/syncedProject/itfdn-140-prod-panksea06/scripts/store.db"

if [ "$#" -eq 0 ] ; then
 more $store_data
else
 # note ${PAGER:-more} allows default pager to be used unless nothing set then more used.
 grep -i "$@" $store_data | ${PAGER:-more}
fi
