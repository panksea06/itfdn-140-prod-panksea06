#!/bin/bash
# Title  store.sh
# Date: 2018/12/10
# Author: Sean Pankanin
# Purpose:  store tips and tricks for later retrival using recall
# Update: 


# variables

#path to store.db data file
store_data="$HOME/repo/syncedProject/itfdn-140-prod-panksea06/scripts/store.db"

# start logic here
if [ "$#" -eq 0 ]; then
   echo "Enter note then quit using Ctrl+D"
   # note - following cat indicates input from keyboard
   cat - >> $store_data
else
   echo "$@" >> $store_data
fi
